import React from "react";
import styled from "styled-components";
import SearchOverlay from "./SearchOverlay";
import ColumnSettingsOverlay from "./ColumnSettingsOverlay";
import AppSettingsOverlay from "./AppSettingsOverlay";

const StyledOverlay = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 2;
    width: 100vw;
    height: 100vh;
    visibility: ${props => props.visible ? "visible" : "hidden"}
`

const StyledBackdrop = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 3;
    background-color: ${props => props.visible ? "var(--Overlay-background)" : "var(--transparent)"};
    opacity: 0.5;
    visibility: ${props => props.visible ? "visible" : "hidden"}
    transition: all 250ms;
`

export default class Overlay extends React.Component {
    render() {
        let overlayContent;
        if (this.props.overlayContentId === "SEARCH") {
            overlayContent = <SearchOverlay visible={this.props.visible} />
        }
        else if (this.props.overlayContentId.startsWith("COLUMN-SETTINGS-")) {
            let columnId = this.props.overlayContentId.split("").pop();
            overlayContent = <ColumnSettingsOverlay visible={this.props.visible} columnId={columnId} />
        }
        else if (this.props.overlayContentId === "App-SETTINGS") {
            overlayContent = <AppSettingsOverlay visible={this.props.visible} />
        }
        else {
            overlayContent = <SearchOverlay visible={this.props.visible} />
        }

        return (
            <StyledOverlay visible={this.props.visible}>
                <StyledBackdrop onClick={this.props.onCloseOverlay} visible={this.props.visible}></StyledBackdrop>
                {overlayContent}
            </StyledOverlay>
        )
    }
}