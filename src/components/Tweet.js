import React from "react";
import styled from "styled-components";
import TweetMetaData from "./TweetMetaData";
import TweetContent from "./TweetContent";
import TweetInteractionOptions from "./TweetInteractionOptions";

const StyledTweet = styled.div`
    display: block;
    width: 100%;
    background-color: var(--Tweet-background);
    border-bottom: solid 1px var(--Tweet-border-bottom);
`

export default class Tweet extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showInteractionOptions: false
        }

        this.mouseEnter = this.mouseEnter.bind(this);
        this.mouseLeave = this.mouseLeave.bind(this);
    }

    mouseEnter() {
        this.setState({
            showInteractionOptions: true
        });
    }

    mouseLeave() {
        this.setState({
            showInteractionOptions: false
        })
    }

    render() {
        return (
            <StyledTweet onMouseEnter={this.mouseEnter} onMouseLeave={this.mouseLeave}>
                <TweetMetaData
                    retweeter={this.props.retweeter}
                    replyingTo={this.props.replyingTo}
                    profilePictureSource={this.props.profilePictureSource}
                    displayName={this.props.displayName}
                    handle={this.props.handle}
                    datetime={this.props.datetime}
                />
                <TweetContent
                    tweetText={this.props.tweetText}
                    hasMedia={this.props.hasMedia}
                />
                <TweetInteractionOptions visible={this.state.showInteractionOptions} />
            </StyledTweet>
        )
    }
}