import React from "react";
import styled from "styled-components";

const StyledColorKeyValuePair = styled.div`
    height: 60px;
    color: ${props => props.visible ? "var(--ColorKeyValuePair-text)" : "var(--transparent)"};
    transition: all 250ms;
`

const StyledColorValueInput = styled.input`
    display: inline-block;
    margin: 7px;
    padding: 2px;
    min-width: 40px;
    min-height: 40px;
    border: solid 1px #000000;
    opacity: ${props => props.visible ? "1" : "0"};
    transition: all 250ms;

    &:hover {
        cursor: pointer;
    }

    &:focus {
        outline: none;
    }
`

const StyledColorKey = styled.div`
    display: inline-block;
    margin: 0 4px;
    line-height: 60px;
    font-size: 16px;
    vertical-align: top;
`

export default class ColorKeyValuePair extends React.Component {
    constructor(props) {
        super(props);

        this.onChangeColor = this.onChangeColor.bind(this);
    }

    onChangeColor(event) {
        this.props.setColor(event.target.value);
    }

    render() {
        return (
            <StyledColorKeyValuePair visible={this.props.visible}>
                <StyledColorValueInput type="color" visible={this.props.visible} value={this.props.color} onChange={this.onChangeColor} />
                <StyledColorKey>{this.props.text}</StyledColorKey>
            </StyledColorKeyValuePair>
        )
    }
}