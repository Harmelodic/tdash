import React from "react";
import styled from "styled-components";

const BasicStyledHandle = styled.a`
    color: var(--Handle-text);
    text-decoration: none;

    &:hover {
        color: var(--Handle-text-hover);
        cursor: pointer;
        text-decoration: underline;
    }
`

export default class Handle extends React.Component {
    render() {
        return (
            <BasicStyledHandle {...this.props} href={"https://twitter.com/" + this.props.handle} target="_blank">
                @{this.props.handle}
            </BasicStyledHandle>
        )
    }
}