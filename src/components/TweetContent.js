import React from "react";
import TweetText from "./TweetText";
import TweetMedia from "./TweetMedia";
import styled from "styled-components";

const StyledTweetContent = styled.div`
    width: 100%;
    min-height: 30px;
`

export default class TweetContent extends React.Component {
    render() {
        return (
            <StyledTweetContent>
                <TweetText tweetText={this.props.tweetText} />
                <TweetMedia hasMedia={this.props.hasMedia} />
            </StyledTweetContent>
        )
    }
}