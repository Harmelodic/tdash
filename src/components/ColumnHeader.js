import React from "react";
import styled from "styled-components";
import Handle from "./Handle";

const StyledColumnHeader = styled.div`
    display: block;
    width: 100%;
    height: 50px;
    background-color: var(--ColumnHeader-background);
    border-bottom: solid 1px var(--ColumnHeader-border-bottom);
`

const StyledColumnTitle = styled.div`
    display: inline-block;
    width: calc(100% - 65px);
    height: 100%;
    padding-left: 15px;
    line-height: 50px;
    font-size: 20px;
    color: var(--ColumnHeaderTitle-text);
    vertical-align: top;
`

const StyledColumnHeaderHandle = styled(Handle) `
    display: inline-block;
    font-size: 10px;
    padding-left: 5px;
    line-height: 10px;
`

const ColumnOptionsMenuButton = styled.div`
    display: inline-block;
    width: 50px;
    height: 100%;
`

const StyledButtonImage = styled.img`
    width: 40px;
    height: 40px;
    padding: 5px;
    cursor: pointer;
`

export default class ColumnHeader extends React.Component {
    render() {
        return (
            <StyledColumnHeader>
                <StyledColumnTitle>
                    {this.props.title}
                    <StyledColumnHeaderHandle handle={this.props.account} />
                </StyledColumnTitle>
                <ColumnOptionsMenuButton>
                    <StyledButtonImage src="/images/gear.svg" onClick={this.props.onColumnSettingsClick} />
                </ColumnOptionsMenuButton>
            </StyledColumnHeader>
        )
    }
}