import React from "react";
import InteractionReplyButton from "./InteractionReplyButton";
import ReplyBox from "./ReplyBox";
import styled from "styled-components";

const StyledTweetInteractionOptions = styled.div`
    width: 100%;
    min-height: 35px;
    text-align: right;
`

const StyledTweetInteractionOptionsButtons = styled.div`
    width: 100%;
    height: 35px;
    border-bottom: ${props => props.replyBoxOpen ? "dashed 1px var(--TweetInteractionOptionsButtons-border-bottom)" : "dashed 1px var(--transparent)"};
    text-align: right;
    transition: border 250ms;
    visibility: ${props => props.visible || props.replyBoxOpen ? "visible" : "hidden"};
`

export default class TweetInteractionOptions extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            replyBoxOpen: false
        }

        this.changeReplyBoxState = this.changeReplyBoxState.bind(this);
    }

    changeReplyBoxState() {
        this.setState({
            replyBoxOpen: !this.state.replyBoxOpen
        });
    }

    render() {
        return (
            <StyledTweetInteractionOptions>
                <StyledTweetInteractionOptionsButtons replyBoxOpen={this.state.replyBoxOpen} visible={this.props.visible}>
                    <InteractionReplyButton onClick={this.changeReplyBoxState} replyBoxOpen={this.state.replyBoxOpen} />
                </StyledTweetInteractionOptionsButtons>
                <ReplyBox open={this.state.replyBoxOpen} />
            </StyledTweetInteractionOptions>
        )
    }
}