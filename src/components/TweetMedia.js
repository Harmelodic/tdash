import React from "react";
import styled from "styled-components";

const StyledTweetMedia = styled.div`
    width: 100%;
    max-width: 300px;
    margin 10px auto 0 auto;
`

const StyledMediaPreviewWrapper = styled.div`
    padding-left: 60px;
    width: calc(100% - 90px);
    height: 100px;
`

const StyledMediaPreview = styled.div`
    width: 100%;
    height: 100%;
    border-radius: 2px;
    background-color: var(--TweetMediaPreview-background);
    cursor: pointer;
`

export default class TweetMedia extends React.Component {
    render() {
        if (this.props.hasMedia) {
            return (
                <StyledTweetMedia>
                    <StyledMediaPreviewWrapper>
                        <StyledMediaPreview />
                    </StyledMediaPreviewWrapper>
                </StyledTweetMedia>
            )
        }
        else {
            return (<div />)
        }
    }
}