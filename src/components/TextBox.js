import React from "react";
import styled from "styled-components";

const StyledTextBox = styled.div`
    display: inline-block;
    width: ${props => props.open ? "calc(100% - 50px)" : "0"};;
    height: ${props => props.open ? "150px" : "0"};;
    margin: 15px;
    padding: ${props => props.open ? "10px" : "0"};
    background-color: var(--TextBox-background);
    border-radius: 5px;
    transition: all 250ms;
`

const StyledTextArea = styled.textarea`
    width: 100%;
    height: 100%;
    padding: 0;
    border: none;
    background-color: var(--TextBox-background);
    vertical-align: top;
    font-size: ${props => props.open ? "15px" : "0"};
    resize: none;
    transition: all 250ms;

    &:focus {
        outline: none;
    }
`

export default class TextBox extends React.Component {
    constructor(props) {
        super(props);

        this.onChangeText = this.onChangeText.bind(this);
    }

    onChangeText(event) {
        this.props.onChangeText(event.target.value);
    }

    render() {
        return (
            <StyledTextBox open={this.props.open}>
                <StyledTextArea open={this.props.open} placeholder={this.props.placeholder} value={this.props.value} onChange={this.onChangeText}/>
            </StyledTextBox>
        )
    }
}