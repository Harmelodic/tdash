import React from "react";
import styled from "styled-components";
import NewTweetForm from "./NewTweetForm";

const StyledReplyBox = styled.div`
    width: 100%;
    max-height: ${props => props.open ? "300px" : "0"};
    transition: max-height 350ms;
    text-align: right;
`

export default class ReplyBox extends React.Component {
    render() {
        return (
            <StyledReplyBox open={this.props.open}>
                <NewTweetForm open={this.props.open} textareaPlaceholder="Tweet your reply..." buttonText="Reply" />
            </StyledReplyBox>
        )
    }
}