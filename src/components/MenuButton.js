import React from "react";
import styled from "styled-components";

const StyledMenuButton = styled.div`
    width: 39px;
    height: 39px;
    margin: 10px auto;
    cursor: pointer;
    border-radius: 2px;
`

export default class TweetButton extends React.Component {
    render() {
        return (
            <StyledMenuButton {...this.props} />
        )
    }
}