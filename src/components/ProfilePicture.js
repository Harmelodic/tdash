import React from "react";
import styled from "styled-components";
import Store from "../redux/Store";

const StyledProfilePicture = styled.img`
    display: inline-block;
    width: 40px;
    height: 40px;
    border-radius: var(--ProfilePicture-shape);
    margin: 10px;
    margin-bottom: 0px;
`

export default class ProfilePicture extends React.Component {
    render() {
        return (
            <StyledProfilePicture src={this.props.imgSrc} />
        )
    }
}