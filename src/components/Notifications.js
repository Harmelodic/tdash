import React from "react";
import styled from "styled-components";
import Notification from "./Notification";

const StyledNotifications = styled.div`
    width: 100%;
    height: calc(100% - 51px);
    overflow: auto;
`

export default class Notifications extends React.Component {
    render() {
        return (
            <StyledNotifications>
                {
                    this.props.notifications.map((notification, index) => {
                        return (
                            <Notification key={index} />
                        )
                    })
                }
            </StyledNotifications>
        )
    }
}