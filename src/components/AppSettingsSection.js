import React from "react";
import styled from "styled-components";

const StyledAppSettingsSection = styled.div`
    width: 100%;
    border-bottom: 1px dashed var(--AppSettingsSection-border);
`

const StyledAppSettingsSectionHeader = styled.div`
    transition: all 250ms;
    background-color: ${props => props.expanded ? "var(--AppSettingsSectionHeader-background-color-active)" : "var(--transparent)"};
    padding-left: 50px;
    width: calc(100% - 50px);
    height: 50px;
    font-size: 20px;
    line-height: 50px;
    color: var(--AppSettingsSectionHeader-text);
    text-align: left;
    cursor: pointer;
    user-select: none;
`

const StyledAppSettingsSectionBody = styled.div`
    display: ${props => props.expanded ? "block" : "none"};
    padding-left: 100px;
    width: calc(100% - 100px);
    text-align: left;
`

export default class AppSettingsSection extends React.Component {
    render() {
        return (
            <StyledAppSettingsSection>
                <StyledAppSettingsSectionHeader onClick={this.props.onHeaderClick} expanded={this.props.expanded}>{this.props.sectionHeader}</StyledAppSettingsSectionHeader>
                <StyledAppSettingsSectionBody expanded={this.props.expanded} {...this.props} />
            </StyledAppSettingsSection>
        )
    }
}