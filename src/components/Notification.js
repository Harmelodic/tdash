import React from "react";
import styled from "styled-components";

const StyledNotification = styled.div`
    display: block;
    width: 100%;
    min-height: 40px;
    background-color: var(--Notification-background);
    border-bottom: solid 1px var(--Notification-border-bottom);
`

export default class Notification extends React.Component {
    render() {
        return (
            <StyledNotification />
        )
    }
}