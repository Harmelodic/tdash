import React from "react";
import styled from "styled-components";
import OverlayContent from "./OverlayContent";

const StyledColumnSettingsOverlay = styled(OverlayContent) `
    margin-top: 100px;
    width: 50%;
    height: 50%;
    background-color: ${props => props.visible ? "var(--ColumnSettingsOverlay-background)" : "var(--transparent)"};
    border-radius: 5px;
    text-align: center;
    transition: all 250ms;
`

const StyledColumnSettingsOverlayHeader = styled.div`
    display: inline-block;
    padding-top: 20px;
    font-size: 32px;
    color: ${props => props.visible ? "var(--ColumnSettingsOverlayHeader-text)" : "var(--transparent)"};
    text-align: center;
    transition: all 250ms;
`

const StyledColumnId = styled.div`
    font-size: 20px;
    color: ${props => props.visible ? "var(--ColumnSettingsOverlayColumnId)" : "var(--transparent)"};
    text-align: center;
    transition: all 250ms;
`

export default class ColumnSettingsOverlay extends React.Component {
    render() {
        return (
            <StyledColumnSettingsOverlay visible={this.props.visible}>
                <StyledColumnSettingsOverlayHeader visible={this.props.visible}>Column Settings</StyledColumnSettingsOverlayHeader>
                <StyledColumnId visible={this.props.visible}>Column {this.props.columnId}</StyledColumnId>
            </StyledColumnSettingsOverlay>
        )
    }
}