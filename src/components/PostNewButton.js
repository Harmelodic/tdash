import React from "react";
import styled from "styled-components";
import MenuButton from "./MenuButton";

const StyledPostNewButton = styled(MenuButton) `
    background-color: var(--PostNewButton-background);
    border: solid 1px var(--PostNewButton-border);
`

const StyledPostNewButtonSymbol = styled.div`
    width: ${props => props.open ? "29px" : "30px"}; // Required for a graphical hiccup/glitch: When rotating the plus icon, it doesn't stay center
    height: 30px;
    padding: 5px;
    font-size: 26px;
    color: var(--PostNewButtonSymbol-text);
    text-align: center;
    line-height: 30px;
    user-select: none;
    transform: ${props => props.open ? "rotate(45deg)" : "none"};
    transition: transform 250ms;
`

export default class PostNewButton extends React.Component {
    render() {
        return (
            <StyledPostNewButton onClick={this.props.onClick}>
                <StyledPostNewButtonSymbol open={this.props.columnOpen}>&#65291;</StyledPostNewButtonSymbol>
            </StyledPostNewButton>
        )
    }
}