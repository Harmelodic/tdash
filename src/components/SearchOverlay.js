import React from "react";
import styled from "styled-components";
import OverlayContent from "./OverlayContent";

const StyledSearchOverlay = styled(OverlayContent) `
    margin-top: 100px;
    width: 50%;
`

const StyledSearchOverlayTextInput = styled.input`
    width: 100%;
    padding: 30px 60px;
    background-color: ${props => props.visible ? "var(--SearchOverlay-background)" : "var(--transparent)"};
    opacity: 0.92;
    font-size: 36px;
    color: ${props => props.visible ? "var(--SearchOverlay-text)" : "var(--transparent)"};
    border: none;
    border-radius: 5px;
    transition: all 250ms;
    
    &:focus {
        outline: none;
    }

    &::placeholder {
        color: ${props => props.visible ? "var(--SearchOverlay-placeholder)" : "var(--transparent)"};
        transition: color 250ms;
    }
`

export default class SearchOverlay extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            searchValue: ""
        }

        this.onChangeSearchValue = this.onChangeSearchValue.bind(this);
    }

    onChangeSearchValue(event) {
        this.setState({
            searchValue: event.target.value
        })
    }

    componentWillReceiveProps(nextProps) {
        if ((this.props.visible === false) && (nextProps.visible === true)) {
            this.setState({
                searchValue: ""
            })
        }
    }

    render() {
        return (
            <StyledSearchOverlay>
                <StyledSearchOverlayTextInput type="search" visible={this.props.visible} placeholder="Search..." value={this.state.searchValue} onChange={this.onChangeSearchValue}/>
            </StyledSearchOverlay>
        )
    }
}