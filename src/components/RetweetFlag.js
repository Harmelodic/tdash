import React from "react";
import styled from "styled-components";
import Handle from "./Handle";

const StyledRetweetFlag = styled.div`
    display: ${props => props.exists ? "block" : "none"}
    width: 100%;
    height: 15px;
    margin-bottom: 5px;
`

const StyledRetweetFlagText = styled.div`
    display: inline-block;
    padding-top: 6px;
    padding-left: 10px;
    font-size: 11px;
    color: var(--RetweetFlag-text);
`

const StyledRetweetHandle = styled(Handle) `
    
`

export default class RetweetFlag extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            exists: (this.props.retweeter !== undefined)
        }
    }

    render() {
        return (
            <StyledRetweetFlag exists={this.state.exists}>
                <StyledRetweetFlagText>
                    <StyledRetweetHandle handle={this.props.retweeter}/> retweeted:
                </StyledRetweetFlagText>
            </StyledRetweetFlag>
        )
    }
}