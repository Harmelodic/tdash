import React from "react";
import styled from "styled-components";

const StyledUserSearchTextBox = styled.div`
    display: inline-block;
    width: ${props => props.open ? "calc(100% - 50px)" : "0"};
    margin: 15px;
    padding: ${props => props.open ? "10px" : "0"};
    background-color: var(--TextBox-background);
    border-radius: 5px;
    transition: all 250ms;
`

const StyledInput = styled.input`
    width: 100%;
    padding: 0;
    border: none;
    background-color: var(--TextBox-background);
    vertical-align: top;
    font-size: ${props => props.open ? "15px" : "0"};
    resize: none;
    transition: all 250ms;

    &:focus {
        outline: none;
    }
`

export default class UserSearchTextBox extends React.Component {
    render() {
        return (
            <StyledUserSearchTextBox open={this.props.open}>
                <StyledInput open={this.props.open} placeholder="To" />
            </StyledUserSearchTextBox>
        )
    }
}