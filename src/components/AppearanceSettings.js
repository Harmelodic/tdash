import React from "react";
import styled from "styled-components";
import { Store } from "../redux/Store";
import Actions from "../redux/Actions";

const StyledTextInputSection = styled.div`
    height: 60px;
    color: ${props => props.visible ? "var(--ColorKeyValuePair-text)" : "var(--transparent)"};
    transition: all 250ms;
`

const StyledTextInput = styled.input`
    display: inline-block;
    margin: 12px;
    margin-right: 0;
    padding: 5px 10px;
    width: 60px;
    height: 25px;
    border: solid 1px #000000;
    border-radius: 3px;
    font-size: 16px;
    transition: all 250ms;

    &:hover {
        cursor: pointer;
    }

    &:focus {
        outline: none;
    }
`

const StyledInputUnitsLabel = styled.div`
    display: inline-block;
    margin: 0 4px;
    line-height: 60px;
    font-size: 16px;
    vertical-align: top;
    color: var(--InputUnitsLabel-text-color);
`

const StyledInputLabel = styled.div`
    display: inline-block;
    margin: 0 4px;
    line-height: 60px;
    font-size: 16px;
    vertical-align: top;
`

const StyledRangeInput = styled.input`
    display: inline-block;
    margin: 12px;
    margin-right: 0;
    padding: 5px 0;
    width: 100px;
    height: 25px;
    border: solid 1px #000000;
    border-radius: 3px;
    font-size: 16px;
    transition: all 250ms;

    &:hover {
        cursor: pointer;
    }

    &:focus {
        outline: none;
    }
`

export default class AppearanceSettings extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            appearance: Store.getState().theme.appearance
        }

        Store.subscribe(() => {
            this.setState({
                appearance: Store.getState().theme.appearance
            })
        });

        this.setColumnWidth = this.setColumnWidth.bind(this);
    }

    setColumnWidth(event) {
        let value = "".concat(event.target.value.toString(), "px");
        Store.dispatch(Actions.updateThemeAppearance("Column-width", value))
    }

    setProfilePictureShape(event) {
        let value = "".concat(event.target.value.toString(), "px");
        Store.dispatch(Actions.updateThemeAppearance("ProfilePicture-shape", value))
    }

    render() {
        return (
            <div>
                <StyledTextInputSection visible={this.props.visible}>
                    <StyledTextInput type="number" onChange={this.setColumnWidth}
                        value={this.state.appearance["Column-width"].toString().replace("px", "")} />
                    <StyledInputUnitsLabel>px</StyledInputUnitsLabel>
                    <StyledInputLabel>Column-width</StyledInputLabel>
                </StyledTextInputSection>
                <StyledTextInputSection visible={this.props.visible}>
                    <StyledRangeInput type="range" onChange={this.setProfilePictureShape}
                        value={this.state.appearance["ProfilePicture-shape"].toString().replace("px", "")}
                        min="0"
                        max="25" />
                    <StyledInputUnitsLabel>(Square &lt;&gt; Circle)</StyledInputUnitsLabel>
                    <StyledInputLabel>ProfilePicture-shape</StyledInputLabel>
                </StyledTextInputSection>
            </div>
        )
    }
}