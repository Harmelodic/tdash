import React from "react";
import RetweetFlag from "./RetweetFlag";
import ProfilePicture from "./ProfilePicture";
import DisplayName from "./DisplayName";
import Handle from "./Handle";
import TweetTime from "./TweetTime";
import ReplyingToFlag from "./ReplyingToFlag";
import styled from "styled-components";

const StyledTweetMetaData = styled.div`
    width: 100%
    min-height: 50px;
    margin-bottom: 10px;
`

const StyledTweetUser = styled.div`
    display: inline-block;
    width: calc(100% - 130px);
    min-height: 50px;
    vertical-align: top;
`

const StyledHandle = styled(Handle) `
    display: inline-block;
    font-size: 10px;
    margin-top: 1px;
`

export default class TweetMetaData extends React.Component {
    render() {
        return (
            <StyledTweetMetaData>
                <RetweetFlag retweeter={this.props.retweeter} />
                <ProfilePicture imgSrc={this.props.profilePictureSource} />
                <StyledTweetUser>
                    <DisplayName displayName={this.props.displayName} />
                    <StyledHandle handle={this.props.handle} />
                </StyledTweetUser>
                <TweetTime datetime={this.props.datetime} />
                <ReplyingToFlag replyingTo={this.props.replyingTo} />
            </StyledTweetMetaData>
        )
    }
}