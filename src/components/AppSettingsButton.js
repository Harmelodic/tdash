import React from "react";
import styled from "styled-components";
import MenuButton from "./MenuButton";

const StyledAppSettingsButton = styled(MenuButton) `
    background-color: var(--transparent);
    border: solid 1px var(--transparent);
`

const StyledAppSettingsButtonSymbol = styled.img`
    width: 40px;
    height: 40px;
    cursor: pointer;
`

export default class AppSettingsButton extends React.Component {
    render() {
        return (
            <StyledAppSettingsButton onClick={this.props.onClick}>
                <StyledAppSettingsButtonSymbol src="/images/gear.svg" />
            </StyledAppSettingsButton>
        )
    }
}