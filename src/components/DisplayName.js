import React from "react";
import styled from "styled-components";

const StyledDisplayName = styled.div`
    font-size: 16px;
    color: var(--DisplayName-text);
    padding-top: 10px;
    white-space: normal;
    overflow-wrap: break-word; // CSS3 version of word-wrap
`

export default class DisplayName extends React.Component {
    render() {
        return (
            <StyledDisplayName>
                {this.props.displayName}
            </StyledDisplayName>
        )
    }
}