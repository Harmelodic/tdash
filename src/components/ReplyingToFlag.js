import React from "react";
import styled from "styled-components";
import Handle from "./Handle";

const StyledReplyingToFlag = styled.div`
    display: ${props => props.exists ? "block" : "none"}
    width: 100%;
    height: 15px;
`

const StyledReplyingToFlagText = styled.div`
    display: inline-block;
    padding-top: 2px;
    padding-left: 60px;
    font-size: 11px;
    color: var(--ReplyingToFlag-text);
`

const StyledReplyingToHandle = styled(Handle)`
`

export default class RetweetFlag extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            exists: (this.props.replyingTo !== undefined)
        }
    }

    render() {
        return (
            <StyledReplyingToFlag exists={this.state.exists}>
                <StyledReplyingToFlagText>
                    replying to: <StyledReplyingToHandle handle={this.props.replyingTo}/>
                </StyledReplyingToFlagText>
            </StyledReplyingToFlag>
        )
    }
}