import React from "react";
import styled from "styled-components";
import ColumnHeader from "./ColumnHeader";
import Tweets from "./Tweets";
import Notifications from "./Notifications";

const StyledColumn = styled.div`
    display: inline-block;
    width: var(--Column-width);
    height: 100%;
    background-color: var(--Column-background);
    border-right: solid 1px var(--Column-border-right);
    vertical-align: top;
`

export default class Column extends React.Component {
    constructor(props) {
        super(props);

        this.onColumnSettingsClick = this.onColumnSettingsClick.bind(this);
    }

    onColumnSettingsClick() {
        this.props.onColumnSettingsClick(this.props.columnId);
    }

    render() {
        let columnContentList;

        switch (this.props.columnContentType) {
            case "TWEET":
                columnContentList = <Tweets tweets={this.props.columnContent} />
                break;
            case "NOTIFICATION":
                columnContentList = <Notifications notifications={this.props.columnContent} />
                break;
            default:
                columnContentList = <div />
                break;
        }

        return (
            <StyledColumn>
                <ColumnHeader
                    title={this.props.title}
                    account={this.props.account}
                    onColumnSettingsClick={this.onColumnSettingsClick} />
                {columnContentList}
            </StyledColumn>
        )
    }
}