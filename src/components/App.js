import React from "react";
import MenuColumn from "./MenuColumn";
import PostNewColumn from "./PostNewColumn";
import Column from "./Column";
import styled from "styled-components";
import Overlay from "./Overlay";
import { Store } from "../redux/Store";

const StyledApp = styled.div`
    height: 100vh;
    background-color: var(--App-background);
`

export default class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            NewTweetColumnOpen: false,
            columns: Store.getState().columns,
            overlayVisible: false,
            overlayContentId: "SEARCH"
        }

        Store.subscribe(() => {
            this.setState({
                columns: Store.getState().columns,
            })
        })

        this.changeNewTweetColumnState = this.changeNewTweetColumnState.bind(this);
        this.onChangeOverlayVisibility = this.onChangeOverlayVisibility.bind(this);
        this.setOverlayContentId = this.setOverlayContentId.bind(this);
        this.onClickSearch = this.onClickSearch.bind(this);
        this.onColumnSettingsClick = this.onColumnSettingsClick.bind(this);
        this.onClickAppSettings = this.onClickAppSettings.bind(this);
    }

    changeNewTweetColumnState() {
        this.setState({
            NewTweetColumnOpen: !this.state.NewTweetColumnOpen
        });
    }

    onChangeOverlayVisibility() {
        this.setState({
            overlayVisible: !this.state.overlayVisible
        })
    }

    setOverlayContentId(contentId) {
        this.setState({
            overlayContentId: contentId
        })
    }

    onClickSearch() {
        this.setOverlayContentId("SEARCH");
        this.onChangeOverlayVisibility();
    }

    onColumnSettingsClick(columnKey) {
        this.setOverlayContentId("COLUMN-SETTINGS-" + columnKey);
        this.onChangeOverlayVisibility();
    }

    onClickAppSettings(){
        this.setOverlayContentId("App-SETTINGS");
        this.onChangeOverlayVisibility();
    }

    render() {
        return (
            <StyledApp>
                <MenuColumn
                    onClickNewTweet={this.changeNewTweetColumnState}
                    columnOpen={this.state.NewTweetColumnOpen}
                    onClickSearch={this.onClickSearch}
                    onClickAppSettings={this.onClickAppSettings} />
                <PostNewColumn open={this.state.NewTweetColumnOpen} />
                {
                    this.state.columns.map((column, index) => {
                        return (
                            <Column
                                key={index}
                                columnId={index}
                                title={column.title}
                                account={column.account}
                                onColumnSettingsClick={this.onColumnSettingsClick}
                                columnContentType={column.contentType}
                                columnContent={column.content}/>
                        )
                    })
                }
                <Overlay
                    visible={this.state.overlayVisible}
                    onCloseOverlay={this.onChangeOverlayVisibility}
                    overlayContentId={this.state.overlayContentId} />
            </StyledApp>
        );
    }
}
