import React from "react";
import ColorKeyValuePair from "./ColorKeyValuePair";
import { Store } from "../redux/Store";
import Actions from "../redux/Actions";

export default class ColorSettings extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            colors: Store.getState().theme.colors
        }

        Store.subscribe(() => {
            this.setState({
                colors: Store.getState().theme.colors
            })
        })
    }

    render() {
        return (
            <div>
                {
                    Object.keys(this.state.colors).map(colorKey => {
                        return (
                            <ColorKeyValuePair
                                key={colorKey}
                                visible={this.props.visible}
                                text={colorKey}
                                setColor={(colorValue) => Store.dispatch(Actions.updateThemeColor(colorKey, colorValue))}
                                color={this.state.colors[colorKey]} />
                        )
                    })
                }
            </div>
        )
    }
}