import React from "react";
import styled from "styled-components";
import MenuButton from "./MenuButton";

const StyledSearchButton = styled(MenuButton) `
    background-color: var(--transparent);
    border: solid 1px var(--SearchButton-border);
`

const StyledSearchButtonSymbol = styled.div`
    width: 16px;
    height: 16px;
    padding: 5px;
    font-size: 28px;
    color: var(--SearchButton-symbol);
    text-align: center;
    line-height: 30px;
    user-select: none;
    transform: rotate(-45deg);
`

export default class SearchButton extends React.Component {
    render() {
        return (
            <StyledSearchButton onClick={this.props.onClick}>
                <StyledSearchButtonSymbol>&#9906;</StyledSearchButtonSymbol>
            </StyledSearchButton>
        )
    }
}