import React from "react";
import styled from "styled-components";

const StyledTweetText = styled.div`
    font-size: 14px;
    color: var(--TweetText-text);
    padding: 0 20px 0 60px;
    white-space: normal;
`

export default class TweetText extends React.Component {
    render() {
        return (
            <StyledTweetText dangerouslySetInnerHTML={{ __html: this.props.tweetText.replace(/\\n/g, '<br/>') }} />
        )
    }
}