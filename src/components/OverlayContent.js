import React from "react";
import styled from "styled-components";

const StyledOverlayContent = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    z-index: 4;
    margin-left: auto;
    margin-right: auto;
`

// You shouldn't really be creating a component of this class.
// Instead you should be creating something to go inside the Overlay...
// ...and then use styled() to inherit this component to ensure the content sits above the Overlay Backdrop.

export default class OverlayContent extends React.Component {
    render() {
        return (
            <StyledOverlayContent {...this.props}/>
        )
    }
}