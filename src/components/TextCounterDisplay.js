import React from "react";
import styled from "styled-components";

const StyledTextCounterDisplay = styled.div`
    display: inline-block;
    font-size: ${props => props.show ? "14px" : "0"};
    transition: font-size 250ms;
    color: ${props => props.countAboveOrEqualToZero ? "var(--TextCounterDisplay-text)" : "red"};
`

export default class TextCounterDisplay extends React.Component {
    render() {
        return (
            <StyledTextCounterDisplay show={this.props.show} countAboveOrEqualToZero={(280 - this.props.count) >= 0}>{280 - this.props.count}</StyledTextCounterDisplay>
        )
    }
}