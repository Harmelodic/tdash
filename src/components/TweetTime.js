import React from "react";
import styled from "styled-components";

const StyledTime = styled.div`
    display: inline-block;
    width: 60px;
    height: 30px;
    padding: 5px;
    text-align: right;
    vertical-align: top;
`

const TweetTimeString = styled.div`
    font-size: 10px;
    color: var(--TweetTimeString-text);
`

export default class TweetTime extends React.Component {
    constructor(props) {
        super(props);

        const dateObject = new Date(this.props.datetime);
        const currentTime = new Date().getTime();
        const millisecondsAgo = currentTime - this.props.datetime;

        this.state = {
            datetime: this.props.datetime,
            fullYear: dateObject.getFullYear().toString(),
            month: (dateObject.getMonth() + 1) < 10 ? "0" + (dateObject.getMonth() + 1).toString() : (dateObject.getMonth() + 1).toString(),
            date: dateObject.getDate() < 10 ? "0" + dateObject.getDate().toString() : dateObject.getDate().toString(),
            hours: dateObject.getHours().toString(),
            minutes: dateObject.getMinutes().toString()
        }
    }

    render() {
        return (
            <StyledTime>
                <TweetTimeString>{this.state.fullYear + "-" + this.state.month + "-" + this.state.date}</TweetTimeString>
                <TweetTimeString>{this.state.hours + ":" + this.state.minutes}</TweetTimeString>
            </StyledTime>
        )
    }
}