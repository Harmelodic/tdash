import React from "react";
import TextBox from "./TextBox";
import TextCounterDisplay from "./TextCounterDisplay";
import Button from "./Button";

export default class NewTweetForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tweetText: ""
        }

        this.onChangeTweetText = this.onChangeTweetText.bind(this);
    }

    onChangeTweetText(newTweetText) {
        this.setState({
            tweetText: newTweetText
        })
    }
    
    render() {
        return (
            <div>
                <TextBox open={this.props.open} placeholder={this.props.textareaPlaceholder} value={this.state.tweetText} onChangeText={this.onChangeTweetText}/>
                <div />
                <TextCounterDisplay show={this.props.open} count={this.state.tweetText.length} />
                <Button show={this.props.open} text={this.props.buttonText} />
            </div>
        )
    }
}