import React from "react";
import TextBox from "./TextBox";
import Button from "./Button";
import UserSearchTextBox from "./UserSearchTextBox";

export default class NewDMForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            message: "",
        }

        this.onChangeMessage = this.onChangeMessage.bind(this);
    }

    onChangeMessage(newMessage) {
        this.setState({
            message: newMessage
        })
    }

    render() {
        return (
            <div>
                <UserSearchTextBox open={this.props.open} />
                <div />
                <TextBox open={this.props.open} placeholder={this.props.textareaPlaceholder} onChangeText={this.onChangeMessage} value={this.state.message}/>
                <div />
                <Button show={this.props.open} text={this.props.buttonText} />
            </div>
        )
    }
}