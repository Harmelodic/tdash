import React from "react";
import styled from "styled-components";
import Tweet from "./Tweet";

const StyledTweets = styled.div`
    width: 100%;
    height: calc(100% - 51px);
    overflow: auto;
`

export default class Tweets extends React.Component {
    render() {
        return (
            <StyledTweets>
                {
                    this.props.tweets.map(tweet => {
                        return (
                            <Tweet
                                key={tweet.tweetId}
                                tweetId={tweet.tweetId}
                                retweeter={tweet.retweeter}
                                replyingTo={tweet.replyingTo}
                                profilePictureSource={tweet.profilePictureSource}
                                displayName={tweet.displayName}
                                handle={tweet.handle}
                                datetime={tweet.datetime}
                                tweetText={tweet.tweetText}
                                hasMedia={tweet.hasMedia}
                            />
                        )
                    })
                }
            </StyledTweets>
        )
    }
}