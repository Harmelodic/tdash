import React from "react";
import styled from "styled-components";
import OverlayContent from "./OverlayContent";
import AppSettingsSection from "./AppSettingsSection";
import AppearanceSettings from "./AppearanceSettings";
import ColorSettings from "./ColorSettings";

const StyledAppSettingsOverlay = styled(OverlayContent) `
    margin-top: 100px;
    width: 50%;
    height: 80%;
    background-color: var(--AppSettingsOverlay-background);
    border-radius: 5px;
    text-align: center;
    transition: all 250ms;
    opacity: ${props => props.visible ? "1" : "0"}
`

const StyledAppSettingsHeader = styled.div`
    display: inline-block;
    padding: 20px 0;
    font-size: 32px;
    color: var(--AppSettingsHeader-text);
    text-align: center;
    transition: all 250ms;
`

const StyledAppSettingsContainer = styled.div`
    margin: 0 auto;
    width: 100%;
    max-width: 800px;
    max-height: calc(100% - 90px);
    border-top: 1px dashed var(--AppSettingsSection-border);
    overflow: auto;
`

export default class AppSettingsOverlay extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            appearanceExpanded: false,
            colorsExpanded: false
        }

        this.onChangeAppearanceExpanded = this.onChangeAppearanceExpanded.bind(this);
        this.onChangeColorsExpanded = this.onChangeColorsExpanded.bind(this);
    }

    onChangeAppearanceExpanded() {
        this.setState({
            appearanceExpanded: !this.state.appearanceExpanded
        })
    }

    onChangeColorsExpanded() {
        this.setState({
            colorsExpanded: !this.state.colorsExpanded
        });
    }

    render() {
        return (
            <StyledAppSettingsOverlay visible={this.props.visible}>
                <StyledAppSettingsHeader visible={this.props.visible}>App Settings</StyledAppSettingsHeader>
                <StyledAppSettingsContainer>
                    <AppSettingsSection
                        sectionHeader="Appearance"
                        onHeaderClick={this.onChangeAppearanceExpanded}
                        expanded={this.state.appearanceExpanded}>
                        <AppearanceSettings visible={this.props.visible} />
                    </AppSettingsSection>
                    <AppSettingsSection
                        sectionHeader="Colors"
                        onHeaderClick={this.onChangeColorsExpanded}
                        expanded={this.state.colorsExpanded}>
                        <ColorSettings visible={this.props.visible} />
                    </AppSettingsSection>
                </StyledAppSettingsContainer>
            </StyledAppSettingsOverlay>
        )
    }
}