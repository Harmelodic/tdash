import React from "react";
import styled, { css } from "styled-components";
import NewTweetForm from "./NewTweetForm";
import NewDMForm from "./NewDMForm";

const StyledPostNewColumn = styled.div`
    display: inline-block;
    position: relative;
    margin-left: ${props => props.open ? "0" : "-301px"};
    width: 300px;
    height: 100%;
    background-color: var(--PostNewColumn-background);
    border-right: solid 1px var(--PostNewColumn-border-right);
    vertical-align: top;
    transition: margin-left 250ms;
    text-align: right;
`

const StyledPostNewColumnHeader = styled.div`
    display: block;
    width: 100%;
    height: 50px;
    background-color: var(--PostNewColumnHeader-background);
    border-bottom: solid 1px var(--PostNewColumnHeader-border-bottom);
    text-align: left;
`

const StyledPostNewColumnHeaderTitle = styled.div`
    display: inline-block;
    margin-left: 5px;
    height: 100%;
    padding: 0 15px;
    line-height: 50px;
    font-size: 20px;
    color: var(--PostNewColumnHeaderTitle-text);
    background-color: ${props => props.selected ? "var(--PostNewColumnHeaderTitle-background-color-selected)" : "var(--transparent)"};
    vertical-align: top;
    user-select: none;
    cursor: ${props => props.selected ? "normal" : "pointer"}
`

export default class PostNewColumn extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            newTweetSelected: true
        }

        this.selectNewTweet = this.selectNewTweet.bind(this);
        this.selectNewDM = this.selectNewDM.bind(this);
    }

    selectNewTweet() {
        this.setState({
            newTweetSelected: true
        })
    }

    selectNewDM() {
        this.setState({
            newTweetSelected: false
        })
    }

    componentWillReceiveProps(nextProps) {
        if ((this.props.open === false) && (nextProps.open === true)) {
            this.setState({
                newTweetSelected: true
            })
        }
    }

    render() {
        return (
            <StyledPostNewColumn open={this.props.open}>
                <StyledPostNewColumnHeader>
                    <StyledPostNewColumnHeaderTitle selected={this.state.newTweetSelected} onClick={this.selectNewTweet}>Tweet</StyledPostNewColumnHeaderTitle>
                    <StyledPostNewColumnHeaderTitle selected={!this.state.newTweetSelected} onClick={this.selectNewDM}>DM</StyledPostNewColumnHeaderTitle>
                </StyledPostNewColumnHeader>
                {
                    this.state.newTweetSelected ? 
                        <NewTweetForm open={this.props.open} textareaPlaceholder="What's happening?" buttonText="Tweet" />
                    : 
                        <NewDMForm open={this.props.open} textareaPlaceholder="Message" buttonText="Send" />
                }
            </StyledPostNewColumn>
        )
    }
}