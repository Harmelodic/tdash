import React from "react";
import styled from "styled-components";
import PostNewButton from "./PostNewButton";
import SearchButton from "./SearchButton";
import AppSettingsButton from "./AppSettingsButton";

const StyledMenuColumn = styled.div`
    display: inline-block;
    position: relative;
    z-index: 1;
    width: 50px;
    height: 100%;
    background-color: var(--MenuColumn-background);
    border-right: solid 1px var(--MenuColumn-border-right);
`

const StyledBottomComponents = styled.div`
    display: inline-block;
    position absolute;
    z-index: 1;
    bottom: 0;
    width: 50px;
`

export default class MenuColumn extends React.Component {
    render() {
        return (
            <StyledMenuColumn>
                <PostNewButton onClick={this.props.onClickNewTweet} columnOpen={this.props.columnOpen} />
                <SearchButton onClick={this.props.onClickSearch} />
                <StyledBottomComponents>
                    <AppSettingsButton onClick={this.props.onClickAppSettings} />
                </StyledBottomComponents>
            </StyledMenuColumn>
        )
    }
}