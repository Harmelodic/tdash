import React from "react";
import styled from "styled-components";

const StyledInteractionReplyButton = styled.div`
    display: inline-block;
    height: 100%;
    margin-right: 15px;
    line-height: 38px;
    font-size: 16px;
    color: var(--InteractionReplyButton-text);
    color: ${props => props.replyBoxOpen ? "var(--InteractionReplyButton-text-color-active)" : "var(--InteractionReplyButton-text)"};
    cursor: pointer;
    user-select: none;

    &:hover {
        color: var(--InteractionReplyButton-text-color-active);
    }
`

export default class InteractionReplyButton extends React.Component {
    render() {
        return (
            <StyledInteractionReplyButton onClick={this.props.onClick} replyBoxOpen={this.props.replyBoxOpen}>&#8617;</StyledInteractionReplyButton>
        )
    }
}