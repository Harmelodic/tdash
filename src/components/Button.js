import React from "react";
import styled from "styled-components";

const StyledButton = styled.div`
    display: inline-block;
    margin: 0 15px 15px 15px;
    border-radius: 5px;
    padding: ${props => props.show ? "10px" : "0"};
    background-color: var(--Button-background);
    font-size: ${props => props.show ? "14px" : "0"};
    color: var(--Button-text);
    cursor: pointer;
    user-select: none;
    transition: padding 250ms, font-size 250ms, background-color 100ms;

    &:active {
        background-color: var(--Button-background-color-active);
    }
`

export default class Button extends React.Component {
    render() {
        return (
            <StyledButton show={this.props.show}>{this.props.text}</StyledButton>
        )
    }
}