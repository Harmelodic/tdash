import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import { initialiseStore } from "./redux/Store";
import { initialiseTheme } from "./Theme";

initialiseStore();
initialiseTheme();

const appRoot = document.getElementById("app");

ReactDOM.render(
    <App />,
    appRoot
);