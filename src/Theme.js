import { Store } from "./redux/Store";

function updateDocumentWithTheme() {
    let themeState = Store.getState().theme;
    Object.keys(themeState).forEach(themeSection => {
        Object.keys(themeState[themeSection]).forEach(key => {
            document.documentElement.style.setProperty("--" + key, themeState[themeSection][key], "");
        })
    })
}

export const initialiseTheme = () => {
    Store.subscribe(() => {
        updateDocumentWithTheme();
    });
    updateDocumentWithTheme();
}
