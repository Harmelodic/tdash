// Action Types
export const UPDATE_COLOR = "UPDATE_COLOR";
export const UPDATE_APPEARANCE = "UPDATE_APPEARANCE";

// Action Creators
export default class Actions {
    static updateThemeAppearance(property, value) {
        return {
            type: UPDATE_APPEARANCE,
            property: property,
            value: value
        }
    }

    static updateThemeColor(property, value) {
        return {
            type: UPDATE_COLOR,
            property: property,
            value: value
        }
    }
}