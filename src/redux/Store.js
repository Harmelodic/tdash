import { rootReducer } from "./Reducers";
import { createStore } from "redux";

export let Store;

export const initialiseStore = () => {
    Store = createStore(rootReducer, initialState);
}

const initialState = {
    columns: [
        {
            title: "Home",
            account: "Harmelodic",
            contentType: "TWEET",
            content: [
                {
                    tweetId: "1",
                    profilePictureSource: "images/ExampleProfilePic.png",
                    displayName: "Harmelodic",
                    handle: "Harmelodic",
                    datetime: 1488725100000,
                    tweetText: "Finally finished it.\n\nInspiring and insightful.\n\nAlso, gave me a few madeleine moments, which some were pleasant and some were difficult to push through.\nHowever, a book that can do that is worth finishing.",
                    hasMedia: true
                },
                {
                    tweetId: "2",
                    retweeter: "Harmelodic",
                    profilePictureSource: "images/ExampleProfilePic.png",
                    displayName: "Yas(naya) Queen",
                    handle: "BruellaDeville",
                    datetime: 1488725100000,
                    tweetText: "lol, I love Irn Bru, I do.",
                    hasMedia: true
                },
                {
                    tweetId: "3",
                    profilePictureSource: "images/ExampleProfilePic.png",
                    displayName: "Harry Mitchell",
                    handle: "HarryIsLate",
                    datetime: 1488725100000,
                    tweetText: "Hey it's me. Tech The Harry Mitchell Extravaganza Sharky Boi It's Tech Shark"
                },
                {
                    tweetId: "4",
                    profilePictureSource: "images/ExampleProfilePic.png",
                    displayName: "oo ee oo ah ah ting tang walla walla bing bang",
                    handle: "LikeTotallyAmy",
                    datetime: 1488725100000,
                    tweetText: "I have a very long Twitter Display Name and it's frustrating for Harm to understand who I am at a glance.\n\nThen he realises it's very obviously Amy."
                },
                {
                    retweeter: "ThePerkyRiolu",
                    tweetId: "5",
                    replyingTo: "ThePerkyRiolu",
                    profilePictureSource: "images/ExampleProfilePic.png",
                    displayName: "Amazing Filmaker Alex 'Kosky101' Kosky",
                    handle: "Kosky101",
                    datetime: 1488725100000,
                    tweetText: "Fuck you, Perky. You shitty boy.",
                    hasMedia: true,
                },
                {
                    tweetId: "6",
                    profilePictureSource: "images/ExampleProfilePic.png",
                    displayName: "Owen Trett",
                    handle: "ThePerkyRiolu",
                    datetime: 1488725100000,
                    tweetText: "Oh my god, Kosky. Did you just assume my gender?!"
                }
            ]
        },
        {
            title: "Notifications",
            account: "Harmelodic",
            contentType: "NOTIFICATION",
            content: [
                {
                    type: "MENTION"
                },
                {
                    type: "QUOTED_TWEET"
                },
                {
                    type: "RETWEET"
                },
                {
                    type: "LIKE"
                },
                {
                    type: "FOLLOWER"
                },
                {
                    type: "LIST"
                },
                {
                    type: "ACTION_ON_RETWEET"
                },
                {
                    type: "ACTION_ON_MENTION"
                },
                {
                    type: "ACTION_ON_TAGGED"
                }
            ]
        },
        {
            title: "Direct Messages",
            account: "Harmelodic",
            contentType: "DM",
            content: []
        }
    ],
    theme: {
        appearance: {
            "Column-width": "300px",
            "ProfilePicture-shape": "25px"
        },
        colors: {
            "App-background": "#141d27",
            "AppSettingsOverlay-background": "#666666",
            "AppSettingsHeader-text": "#ffffff",
            "AppSettingsSectionHeader-text": "#ffffff",
            "AppSettingsSectionHeader-background-color-active": "#777777",
            "AppSettingsSection-border": "#bbbbbb",
            "Button-background": "#2794d8",
            "Button-background-color-active": "#1778b5",
            "Button-text": "#ffffff",
            "ColorKeyValuePair-text": "#ffffff",
            "Column-background": "#141d27",
            "Column-border-right": "#000000",
            "ColumnHeader-background": "#1B2836",
            "ColumnHeader-border-bottom": "#000000",
            "ColumnHeaderTitle-text": "#ffffff",
            "ColumnSettingsOverlay-background": "#666666",
            "ColumnSettingsOverlayHeader-text": "#ffffff",
            "ColumnSettingsOverlayColumnId": "#dddddd",
            "DisplayName-text": "#ffffff",
            "Handle-text": "#bbbbbb",
            "Handle-text-hover": "#2eadef",
            "InteractionReplyButton-text": "#ffffff",
            "InteractionReplyButton-text-color-active": "#46aeef",
            "InputUnitsLabel-text-color": "#bbbbbb",
            "MenuColumn-background": "#141d27",
            "MenuColumn-border-right": "#000000",
            "Notification-background": "#16212d",
            "Notification-border-bottom": "#000000",
            "Overlay-background": "#000000",
            "PostNewButton-background": "#4792dc",
            "PostNewButton-border": "#4792dc",
            "PostNewButtonSymbol-text": "#ffffff",
            "PostNewColumn-background": "#141d27",
            "PostNewColumn-border-right": "#000000",
            "PostNewColumnHeader-background": "#2c4156",
            "PostNewColumnHeader-border-bottom": "#000000",
            "PostNewColumnHeaderTitle-background-color-selected": "#1e2d3e",
            "PostNewColumnHeaderTitle-text": "#ffffff",
            "ReplyingToFlag-text": "#bbbbbb",
            "RetweetFlag-text": "#bbbbbb",
            "SearchButton-border": "#4792dc",
            "SearchButton-symbol": "#4792dc",
            "SearchOverlay-background": "#ffffff",
            "SearchOverlay-text": "#000000",
            "SearchOverlay-placeholder": "#666666",
            "TextBox-background": "#ffffff",
            "TextCounterDisplay-text": "#ffffff",
            "Tweet-background": "#1B2836",
            "Tweet-border-bottom": "#000000",
            "TweetInteractionOptionsButtons-border-bottom": "#000000",
            "TweetMediaPreview-background": "#808080",
            "TweetText-text": "#ffffff",
            "TweetTimeString-text": "#aaaaaa"
        }
    }
}