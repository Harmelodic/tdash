import { UPDATE_COLOR, UPDATE_APPEARANCE } from "./Actions";
import { combineReducers } from 'redux';

export const rootReducer = (state, action) => {
    return {
        columns: columnsReducer(state.columns, action),
        theme: themeReducer(state.theme, action)
    }
}

const columnsReducer = (columnsState = [], action) => columnsState.map(columnState => columnReducer(columnState, action))

const columnReducer = (columnState = {}, action) => {
    let contentReducer;

    switch (columnState.contentType) {
        case "TWEET":
            contentReducer = tweetsReducer(columnState.content, action);
            break;
        case "NOTIFICATION":
            contentReducer = notificationsReducer(columnState.content, action);
            break;
        default:
            contentReducer = [];
            break;
    }

    return {
        title: columnState.title,
        account: columnState.account,
        contentType: columnState.contentType,
        content: contentReducer
    }
}

const tweetsReducer = (tweetsState = [], action) => tweetsState.map(tweetState => tweetReducer(tweetState, action))

const tweetReducer = (tweetState = {}, action) => {
    return tweetState;
}

const notificationsReducer = (notificationsState = [], action) => notificationsState.map(notificationState => notificationReducer(notificationState, action))

const notificationReducer = (notificationState = {}, action) => {
    return notificationState;
}

const themeReducer = (themeState = {}, action) => {
    return {
        appearance: appearanceReducer(themeState.appearance, action),
        colors: colorsReducer(themeState.colors, action)
    }
}

const appearanceReducer = (appearanceState = {}, action) => {
    if (action.type === UPDATE_APPEARANCE) {
        let newAppearanceState = {};
        Object.keys(appearanceState).forEach(appearanceKey => {
            if (appearanceKey === action.property) {
                newAppearanceState[appearanceKey] = action.value;
            }
            else {
                newAppearanceState[appearanceKey] = appearanceState[appearanceKey];
            }
        })
        return newAppearanceState;
    }
    return appearanceState;
}

const colorsReducer = (colorsState = {}, action) => {
    if (action.type === UPDATE_COLOR) {
        let newColorsState = {};
        Object.keys(colorsState).forEach(colorKey => {
            if (colorKey === action.property) {
                newColorsState[colorKey] = action.value;
            }
            else {
                newColorsState[colorKey] = colorsState[colorKey];
            }
        })
        return newColorsState;
    }
    return colorsState;
}
